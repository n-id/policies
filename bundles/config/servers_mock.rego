package servers.mock

urls := [
    "http://localhost:8181/v1/data/servers/mock/alpha",
    "http://localhost:8181/v1/data/servers/mock/beta",
    "http://localhost:8181/v1/data/servers/mock/gamma"
]

alpha := {
    "data": "alpha"
}

beta := {
    "data": "beta"
}

gamma := {
    "data": "gamma"
}
