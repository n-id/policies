package providers.contact

import rego.v1

# https://play.openpolicyagent.org/p/Y1AWhSAbbF

default users := {"id8837395937": "john.doe@email.com"}

default ia_secret := "ia-contract-secret"

email := item if {
	some key in object.keys(input.claims)
	key == "email"
	input.claims[key].essential == true
	item := users[input.uid]
}

claims := {"access_token": io.jwt.encode_sign(
	{
		"typ": "JWT",
		"alg": "HS256",
	},
	{"email": email},
	{
		"kty": "oct",
		"k": base64url.encode_no_pad(ia_secret),
	},
)}

token := {
	"access_token": io.jwt.encode_sign(
		{
			"typ": "JWT",
			"alg": "HS256",
		},
		{"iss": "contact-issuing-authority-mock"},
		{
			"kty": "oct",
			"k": base64url.encode_no_pad(ia_secret),
		},
	),
	"token_type": "bearer",
} if {
	input.grant_type == "urn:ietf:params:oauth:grant-type:jwt-bearer"
	input.assertion != ""
	jwt_parts := io.jwt.decode(input.assertion)
	count(jwt_parts) == 3
}

token := {"error": "invalid assertion"} if {
	not io.jwt.decode(input.assertion)
}
