package providers.identities

import future.keywords

default first_names := {"id8837395937": "John"}

default last_names := {"id8837395937": "Doe"}

default document_numbers := {"id8837395937": "123-456-789"}

default ia_secret := "ia-identities-secret"

# https://play.openpolicyagent.org/p/Ca2oWpYX0G

first_name := item if {
	some key in object.keys(input.claims)
	key == "first_name"
	input.claims[key].essential == true
	item := first_names[input.uid]
}

last_name := item if {
	some key in object.keys(input.claims)
	key == "last_name"
	input.claims[key].essential == true
	item := last_names[input.uid]
}

document_number := item if {
	some key in object.keys(input.claims)
	key == "document_number"
	input.claims[key].essential == true
	item := document_numbers[input.uid]
}

claims := {"access_token": io.jwt.encode_sign(
	{
		"typ": "JWT",
		"alg": "HS256",
	},
	{
		"first_name": first_name,
		"last_name": last_name,
		"document_number": document_number,
	},
	{
		"kty": "oct",
		"k": base64url.encode_no_pad(ia_secret),
	},
)}


token := {
	"access_token": io.jwt.encode_sign(
		{
			"typ": "JWT",
			"alg": "HS256",
		},
		{"iss": "identities-issuing-authority-mock"},
		{
			"kty": "oct",
			"k": base64url.encode_no_pad(ia_secret),
		},
	),
	"token_type": "bearer",
} if {
	input.grant_type == "urn:ietf:params:oauth:grant-type:jwt-bearer"
	input.assertion != ""
	jwt_parts := io.jwt.decode(input.assertion)
	count(jwt_parts) == 3
}

token := {"error": "invalid assertion"} if {
	not io.jwt.decode(input.assertion)
}