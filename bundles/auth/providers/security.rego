package system.authz

import rego.v1
import data.providers.contact
import data.providers.identities

################
# IA Mock
#
# In order to test the auth-chaining feature using OPA, we need to be able to simulate
# the behavior of the IA. When a token request is not allowed, the IA should return a 401
# this can only be done in OPA using system.authz.allow.
################

default registered_clients := {
	"mock-client-id": {
		"issuer": "auth.nid",
		"secret": "mock-secret",
		"jwks_uri": "http://jwks.nid.svc.cluster.local:8080/jwks",
	}
}

default users_actors := {"id8837395937": ["nid-test-subject"]}

default allow := false

allow if {
    # If the path is not claims or token, allow the request.
	not input.path[count(input.path) - 1] in ["claims", "token"]
}

allow if {
    # If the path is claims, but not an IA, allow the request.
	input.path[count(input.path) - 1] == "claims"
	not input.path[count(input.path) - 2] in ["contact", "identities"]
}

allow if {   
    # This rule only counts for claims requests for the contact IA.
	input.path[count(input.path) - 1] == "claims"
    input.path[count(input.path) - 2] == "contact"

    # Validate the token using the contact IA secret.
    authorization_header := input.headers.Authorization[0]
    startswith(lower(authorization_header), "bearer ")
    token := substring(authorization_header, 7, -1)
    [valid, _,_] := io.jwt.decode_verify(token, {"secret": contact.ia_secret})
    valid == true
}

allow if {
    # This rule only counts for claims requests for the identities IA.
	input.path[count(input.path) - 1] == "claims"
	input.path[count(input.path) - 2] == "identities"

    # Validate the token using the identities IA secret.
	authorization_header := input.headers.Authorization[0]
	startswith(lower(authorization_header), "bearer ")
	token := substring(authorization_header, 7, -1)
	[valid, _,_] := io.jwt.decode_verify(token, {"secret": identities.ia_secret})
	valid == true
}

allow if {
    # This rule only counts for token requests.
	input.path[count(input.path) - 1] == "token"
	input.path[count(input.path) - 2] in ["contact", "identities"]

    # Use the registered client to validate the client credentials.
	authorization_header := input.headers.Authorization[0]
	startswith(lower(authorization_header), "basic ")
	client_auth := substring(authorization_header, 6, -1)
    [client_id, client_secret] := split(base64.decode(client_auth), ":")
    registered_clients[client_id].secret == client_secret

    # Get the JWKS from the registered client.
    response := http.send({
        "method": "GET", 
        "url": registered_clients[client_id].jwks_uri, 
        "force_cache": true, 
        "force_cache_duration_seconds": 3600
    })

    # Verify the JWT assertion using the JWKS.
    verified := io.jwt.verify_rs256(input.body.input.assertion,response.raw_body)
    verified == true

    # Check if the issuer of the JWT assertion is the registered client.
    [_, payload, _] := io.jwt.decode(input.body.input.assertion)
    registered_clients[client_id].issuer == payload.iss
}
