# Auth Bundle

This bundle contains all policies related to authentication and authorization, used by the [Auth Service](https://gitlab.com/n-id/core/-/blob/main/svc/auth/README.md).

There are 3 packages in this bundle:

- [ClaimNames](#claimnames) All policies related to claim names.
- [Providers](#providers) All policies related to providers.
- [Scope](#scope-validation) All policies related to scope validation.

## ClaimNames

Claim names policies are used by the auth service to map scopes to claims.

### ClaimNames Input

The input of a claim names policy is object with the following properties:

| Property    | Type            | Description                                     |
| ----------- | --------------- | ----------------------------------------------- |
| `sub`       | string          | The subject of the request.                     |
| `aud`       | []string        | Array of audiences.                             |
| `client_id` | string          | The client ID.                                  |
| `scopes`    | []string        | Array of requested scopes.                      |
| `act`       | [Actor](#actor) | Actor of the request, if delegation is enabled. |

#### Actor

The actor is the user that is making the request. This can be a user or a service account.

| Property | Type   | Description               |
| -------- | ------ | ------------------------- |
| `sub`    | string | The subject of the actor. |

### ClaimNames Output

The output is an object with a provider URL as key and a array of claim string as value.

## Providers

The Providers package is a temporary mock package for implementing an Issuing Authority.
This package is used to test the functionality of the auth service.
This mock implements the `/token` and `/claims` endpoints.

There are 2 Issuing Authorities (IAs) implemented in this mock: `contact` and `identities`.
Since OPA only allows the `system.authz.allow` package to issue 401 Unauthorized responses,
the security of the Issuing Authorities are located in the [`security.rego`](./security.rego) file and the functionality
of each IA is implemented in its respective file (e.g., [`contact.rego`](./providers/contact.rego)).

### Token Endpoint

The token endpoint is used to obtain access tokens for different domains.
A token signed by nID is used as assertion along with client credentials to verify the authenticity of the requester.
If the assertion is valid and the client credentials are correct, the token endpoint will issue a token of the Issuing Authority.

### Claims Endpoint

The claims endpoint is used to retrieve user claims from different domains.
A token signed by the Issuing Authority is used to authenticate the request.
If the token is valid, the claims endpoint will return the claims of the user.

## Scope validation

Scope Validation policies are used by the auth service to validate the scope of a token. The specific
validation type and package can be configured in the [auth service configuration](https://gitlab.com/n-id/core/-/blob/main/svc/auth/README.md).

### Scope validation Input

The input of a scope validation policy is object with the following properties:

| Property    | Type            | Description                                                  |
| ----------- | --------------- | ------------------------------------------------------------ |
| `sub`       | string          | The subject of the request.                                  |
| `aud`       | []string        | Array of audiences.                                          |
| `client_id` | string          | The client ID.                                               |
| `scopes`    | []string        | Array of requested scopes.                                   |
| `act`       | [Actor](#actor) | (If present) Actor of the request, if delegation is enabled. |

### Scope validation Output

The output of a scope validation policy a result object with the following variables:

| Variable       | Type              | Description                                |
| -------------- | ----------------- | ------------------------------------------ |
| `allowed`      | boolean           | Whether the request is allowed or not.     |
| `valid_scopes` | []string          | List of valid scopes.                      |
| `errors`       | [Errors](#errors) | The HTTP status code returned to the user. |

#### Errors

Errors is an array of objects with the following structure:

| Variable  | Type   | Description                         |
| --------- | ------ | ----------------------------------- |
| `message` | string | The error message.                  |
| `scope`   | string | The scope the message is regarding. |
