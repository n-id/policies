package claimnames

import future.keywords

# https://play.openpolicyagent.org/p/7NZTRqG4CG

default provider_url := "http://localhost:8181/v1/data/providers/identities/claims"

default another_provider_url := "http://localhost:8181/v1/data/providers/contact/claims"

result[key] := value if {
	some key in object.keys(claims)
	value := [item | item := claims[key][_][_]]
}

claims[provider_url] contains {"first_name", "last_name"} if {
	"person" in input.scopes
}

claims[provider_url] contains {"document_number"} if {
	"person" in input.scopes
}

claims[another_provider_url] contains {"email"} if {
	"contact" in input.scopes
}
