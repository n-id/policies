package scope.validation

import rego.v1

# https://play.openpolicyagent.org/p/Z3EIFQSXpL

default result["allowed"] := false

result["allowed"] if {
	count(errors) == 0
}

result["errors"] := errors

result["valid_scopes"] := [scope |
	input_scope := input.scopes[_]
	every error in errors {
		error.scope != input_scope
	}

	scope := input_scope
]

errors contains msg if {
	some i
	input.scopes[i] == "deny"

	msg := {
		"message": "The request was denied by the policy due to the deny scope.",
		"scope": "deny",
	}
}
