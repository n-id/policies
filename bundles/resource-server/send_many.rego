package resource_server.send_many

import rego.v1
import data.config

server_urls := http.send({
    "method": "GET",
    "url": config.CONFIG_URL
})

responses := http.send_many([ 
    {
        "method": "GET",
        "url": url
    } | url = server_urls.body.result[_] 
])

results := [resp.body.result | resp = responses[_]]