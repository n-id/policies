package authz

import rego.v1

default result["allowed"] := false

default result["http_status"] := 200

result["body"] := json.marshal({"errors": deny})

result["allowed"] if {
	count(deny) == 0
}

result["headers"] := {
	"content-type": "application/json"
}

result["http_status"] := 500 if {
	some error in deny
	error.extensions.code == errors.INTERNAL_SERVER_ERROR
} else := 401 if {
	some error in deny
	error.extensions.code == errors.UNAUTHORIZED
} else := 400 if {
	some error in deny
	error.extensions.code == errors.BAD_REQUEST
} else := 403 if {
	some error in deny
	error.extensions.code == errors.FORBIDDEN
}

deny contains msg if {
	input.parsed_body.deny == true
	msg := {
		"message": "This is the error message",
		"extensions": {"code": errors.INTERNAL_SERVER_ERROR},
	}
}

# HELPER Errors.
errors := {
	"INTERNAL_SERVER_ERROR": "INTERNAL_SERVER_ERROR",
	"UNAUTHORIZED": "UNAUTHORIZED",
	"BAD_REQUEST": "BAD_REQUEST",
	"FORBIDDEN": "FORBIDDEN",
}

dynamic_metadata := {"foo": "bar"}
