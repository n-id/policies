# Resource Server Bundle

The policies in this bundle make up the authorization rules for the Resource Server.

For more information about the Resource Server, see the [Resource Server documentation](https://gitlab.com/n-id/core/-/blob/main/charts/resource-server/README.md).

The policies in this bundle are for demonstration purposes only and should be replaced with the actual policies when implementing a Resource Server.

## Config Bundle

As a demonstration, this repository contains a config bundle.
Using a config bundle is not necessary for almost all use cases, but, in the case where the policy needs to access data from an external source and the bundle is used in different environments (eg. dev, test, acceptance, production), a config bundle can be used to make deterministic policy servers.

In the example config bundle found in [../config](../config), a config url is defined with urls that contain data that is needed by the policy.
Both the bundles are loaded into opa via the [bundles configuration](../../example.config.yaml) and can be referenced as [`data.config.CONFIG_URL`](./send_many.rego#L4) in the policy.

## Input

The input for the demonstration policies is the following object:

| Property      | Type                        | Description                  |
| ------------- | --------------------------- | ---------------------------- |
| `parsed_body` | [Parsed Body](#parsed-body) | An Object send by the input. |

### Parsed Body

The parsed body is the body of the request parsed into an object.

| Property | Type    | Description                                                                     |
| -------- | ------- | ------------------------------------------------------------------------------- |
| `deny`   | boolean | If true, the request will return 500 internal server error to deny the request. |

## Output

The output of the demonstration policies is the following object as described in the [Policy Design](../../POLICY_DESIGN.md) documentation:

| Property      | Type    | Description                                  |
| ------------- | ------- | -------------------------------------------- |
| `allowed`     | boolean | Whether the request is allowed or not.       |
| `http_status` | number  | The HTTP status code returned to the user.   |
| `body`        |         | The HTTP response body returned to the user. |

### Output body

| Property | Type               | Description        |
| -------- | ------------------ | ------------------ |
| `errors` | [] [Error](#error) | The error message. |

### Error

| Property  | Type   | Description        |
| --------- | ------ | ------------------ |
| `message` | string | The error message. |
| `code`    | string | The error code.    |
