package system.log

import rego.v1

mask contains {
	"op": "upsert",
	"path": "/input/attributes/request/http/headers/authorization",
	"value": "**REDACTED**",
} if {
	input.input.attributes.request.http.headers.authorization
}