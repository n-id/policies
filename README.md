# ![logo](./assets/logo-144x144.png) Policies

This repository contains all authorization policies used within nID.
These policies, written in Rego, enable Open Policy Agent (OPA) to decide whether a client has access to the requested resources.

There are 2 bundles in this repository:

- [Auth](./bundles/auth/README.md) All policies related to authentication and authorization, used by the [Auth Service](https://gitlab.com/n-id/core/-/blob/main/svc/auth/README.md).
- [Resource Server](./bundles/resource-server/README.md) All policies related to the resource server, used by the [Resource Server](https://gitlab.com/n-id/core/-/blob/main/charts/resource-server/README.md).

For more information on policy design, see the [Policy Design](./POLICY_DESIGN.md) documentation.

## Policy Integrity Monitoring with Open Policy Agent (OPA)

## Introduction

OPA runs within nID and is therefore part of the authentication and authorization chain. Because managing the authorization policies is separate from executing the authorization, it was decided to manage the policies in a separate repository. The policies can therefore be adjusted independently of nID and have their own change lifecycle.

Splitting the policies and the implementation and handing over the management of the policies to third parties entails integrity risks. Below is a brief summary of why it is important to mitigate this, followed by a detailed explanation of what risks there are and how they are mitigated.

- **Security**: Signing policies prevents manipulation and guarantees their security.
- **Trust**: Digital signatures provide end users with the assurance that the policies come from a reliable source.
- **Compliance**: In many environments, maintaining strict security protocols and compliance standards is essential.
- **Automation and Efficiency**: Automatic signing streamlines workflows and reduces human errors.

### Using OPA instead of cosign/policy

<!-- This documentation provides a comprehensive overview of the bundle signing process within Open Policy Agent (OPA). It focuses on the steps, components, and security protocols involved in ensuring the integrity and authenticity of OPA policy bundles. -->

### Trust Boundaries

Within this system there are two important trust boundaries:

- Adjusting policies by third parties crosses a trust boundary.
- OPA's retrieval of policies from the repository exceeds a trust limit.

![chart](./assets/policy-management.png 'Policy Management')

When crossing a trust boundary, there is a risk that the content of the policies will be compromised, so it is important to mitigate these risks.

### Risks and Measures

#### Adjustment of Policies by Third Parties

Changing the policies can be outsourced to the manager of the resource servers (third party). To make this as easy as possible, we decided to use a Git repository on Gitlab. Third parties can manage their policies themselves, any request to change or add a policy will have to be checked by an admin of this repository. Third parties therefore have no rights to immediately implement their changes.

When changes are implemented by a third party, policies are sent from this person's environment to the online Gitlab environment, a trust boundary is crossed and there is therefore a security risk.

The following two risks have been identified:

##### Access by unauthorized accounts

To prevent this, the repository is set up in such a way that only authorized accounts have access to make changes.

#### Perform transactions under the name of another authorized user

For auditability it is important that it is known who makes which changes to a policy, so it is important that people can only make changes under their own name. To guarantee this, you are required to use a GPG key. Every mutation is cryptographically signed with the GPG key.

#### Synchronizing bundles between the repository and OPA

The OPA service will have to periodically synchronize with the policy repository, which also involves crossing a trust boundary. It is therefore important that the OPA service can "trust" that the policies it retrieves come from the correct source without having been adjusted in the meantime.

To guarantee this, we use "bundle signing". This allows the OPA service to cryptographically verify that the policies packaged in a bundle originate from the repository and that the contents have not been tampered with.

This process is as follows: when policies are changed, the repository administrator can choose to create a new bundle. This bundle contains all the latest versions of the various policies. The created bundle is signed within the repository with a Private key. The OPA service retrieves the latest version of the bundle and checks whether the signature comes from the Gitlab repository, the OPA service does this using the Public key of the repository
