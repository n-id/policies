# Policy design

To ensure the quality of the policies, there are a number of rules that must be followed.
These keep the code readable and consistent, but simplify the usage of the policies.

<!--toc-->

- [Policy design](#policy-design)
  - [Result](#result)
    - [Allowed](#allowed)
    - [HTTP status](#http-status)
    - [Body](#body)
  - [Functional error messages](#functional-error-messages)
    - [Deny rules](#deny-rules)
    - [Error structure](#error-structure)
  - [Bundle Management](#bundle-management)
  - [Decision logs](#decision-logs)
    - [Masking Sensitive Data](#masking-sensitive-data)

<!-- tocstop -->

## Result

Because we use different variables and tools to write the policies it is important to know what the result of a policy is.
The result of the policy must be a object is called 'result'. This object must contain the following variables:

| Variable      | Type    | Description                                  |
| ------------- | ------- | -------------------------------------------- |
| `allowed`     | boolean | Whether the request is allowed or not.       |
| `http_status` | number  | The HTTP status code returned to the user.   |
| `body`        | string  | The HTTP response body returned to the user. |

This makes it possible to only get the necessary output when evaluating the policy and not unnecessary variables.

The result can easily be constructed with other variables such as in the `example.rego`:

```rego
default result["allowed"] := false

result["body"] := json.marshal({"errors": deny})

result["allowed"] if {
  count(deny) == 0
}

result["headers"] := {
  "content-type": "application/json"
}

result["status"] := 500
```

### Allowed

OPA policies are used to determine whether a request is allowed or not.
To do this, the `allowed` variable is used.
This variable should always be used to indicate whether a request is allowed or not.
Because this variable must always be present it's a good idea to define a default value.
This can be done in the following way:

```rego
default result["allowed"] := false
```

### HTTP status

By returning an HTTP status code in the result, the user gets the most information about the request.
It is therefore a good idea to define a default value for the `http_status`.
This can be done in the following way:

```rego
# 200 is the status code for OK
default result["http_status"] := 200
```

If a different status code is needed, it can be adjusted. This can be done next
way:

```rego
result["http_status"] := 401 if {
  not is_token_valid
}
```

### Body

The body is also available in the result. It is good to have a standard way of returning the body.
For graphql api's the body should be a object with an [`errors` array](#error-structure). Something like the following:

```json
{
  "errors": [
    {
      "message": "A where for 'setting' is required and must be set to 123456.",
      "extensions": {
        "code": "BAD_REQUEST"
      }
    }
  ]
}
```

The body of the result must be a string, so it must be marshalled to a string.
Since we return json it is also a good idea to return the content type in the headers.

```rego
result["body"] := json.marshal({"errors": deny})

result["headers"] := {
  "content-type": "application/json"
}
```

## Functional error messages

Functional error messages are incredibly helpful to the user.
Deny rules are the easiest way to return as much information as possible to the user.

### Deny rules

With deny rules it is easy to build up functional error messages.
Each `deny` rule looks at a specific part of the incoming request and determines whether this is allowed or not.
When the request does not comply with the rules, an error message is added to the `deny` array.
This array is then returned to the user.

A `deny` rule looks like this:

```rego
deny contains msg if {
     some i, j
     query_ast.Operations[i].SelectionSet[j].Alias == "ships"
     not where_setting_required(query_ast.Operations[i].SelectionSet[j], "123456")
     msg := {
         "message": err("A where for 'setting' is required and must be set to 123456.", i),
         "extensions": {"code": errors.INTERNAL_SERVER_ERROR},
     }
}
```

The first line checks whether the rule applies to the current request, in this case checking if the ships field is requested.
Then it executes the `where_setting_required` function, which checks whether the `where` is set to `123456`.
If this is not the case, an error message is added to the `deny` array.

### Error structure

An error has a number of fixed components. These parts are:

| Variable     | Description                                                      |
| ------------ | ---------------------------------------------------------------- |
| `message`    | The error message returned to the user.                          |
| `extensions` | The error extensions, extra information about the type of error. |

The `message` is a string returned to the user. This string can ben built up
using the `err` function, this will nicely build up the error. An error looks like this:
out:

```rego
     msg := {
         "message": err("A where for 'setting' is required and must be set to 123456.", i),
         "extensions": {"code": errors.INTERNAL_SERVER_ERROR},
     }
```

## Bundle Management

Since opa packages are not reliant on directories, it is possible to bundle a package that is split across multiple directories. This is useful for organizing a package into multiple files, or for sharing a package that is split across multiple repositories.

We use this technique to organize the entrypoint of our bundle, the `authz` package.
Each package that wishes to be part of the `authz` package can create a `main.rego` file in their package directory, and then import their package into the `authz` package.
There are a few rules that must be followed to make this work:

1. The package must be imported into the `authz` package.
2. The rules that are added to the `authz` package must evaluate only when the request is aimed for that package.
   - This is done by checking the scopes in the token.

Take the following package for example, this adds the deny messages of `yourpackage` to the `authz` package only when all scopes begin with `yourpackage:`:

```rego
package authz

# File: yourpackage/main.rego

import data.yourpackage

deny contains msg if {
    # Make sure it only runs 
    every scope in token.scopes {
        startswith(scope, "yourpackage:")
    }

    some msg in data.yourpackage.deny
}
```

## Decision logs

Every decision made by the policy engine is logged. This is useful for debugging and auditing. The decision logs contain the input, result, data and policy revision.
Make sure decisionlogs are enabled in the OPA configuration.

```yaml
decision_logs:
  enabled: true
```

### Masking Sensitive Data

In the input of a policy, there can be sensitive data. This data should not be logged.
To prevent this, the sensitive data can be masked. This must be done in a seperate package `system.log`.
To do this in a way that is easy to read and audit, the mask should be an `upsert` operation the `\*\*REDACTED\*\*` value.
The mask body should check if the sensitive data is present, like so:

```rego
package system.log

import rego.v1

mask contains {
  "op": "upsert",
  "path": "/input/attributes/request/http/headers/authorization",
  "value": "**REDACTED**",
} if {
  input.input.attributes.request.http.headers.authorization
}
```
